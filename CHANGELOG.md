# Changelog
All notable changes to iquaview lib are documented in this file.

## [20.10.6] - 2021-11-19
### Fixed
* The actions to be called in the xml.

## [20.10.5] - 2021-09-06
### Added
* Support for Iqua Strobe Lights.

## [20.10.4] - 2021-05-31
### Added
* Support for Norbit WBMS Multibeam.

## [20.10.3] - 2021-04-08
### Fixed
* Increased the timeout in the method: send trigger service.

## [20.10.2] - 2021-03-30
### Fixed
* Some cola2 interface functions return a dictionary instead of the result as a boolean.

## [20.10.1] - 2021-03-26
### Added
* The timeout parameter added in some cola2 interface methods.

## [20.10.0] - 2020-10-23

* First Release version.

