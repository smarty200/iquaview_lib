# -*- coding: utf-8 -*-
"""
Copyright (c) 2020 Iqua Robotics SL

# Distributed under the terms of the Modified BSD License (BSD 3-clause)
# see 'LICENSE.txt', which is part of this source code package.
"""

"""
 Class to handle the loading/saving of the different IquaView configuration parameters
"""

import yaml as yaml
import os.path
import logging
from shutil import copytree

logger = logging.getLogger(__name__)

DEFAULTCONFIG = {'canvas_marker_mode': 'auto',
                 'canvas_marker_scale': 400,
                 'configs_path': '/auv_configs',
                 'coordinate_format': 'degree',
                 'data_output_connection': {},
                 'default_project_name': 'guiproject.qgs',
                 'gps_configure_heading': False,
                 'gps_ip': '127.0.0.1',
                 'gps_heading_ip': '127.0.0.1',
                 'gps_hdt_port': 4000,
                 'gps_gga_port': 4000,
                 'gps_protocol': 'TCP',
                 'gps_heading_protocol': 'TCP',
                 'gps_serial': False,
                 'gps_heading_serial': False,
                 'gps_serial_baudrate': 4800,
                 'gps_serial_port': 'internalGPS',
                 'gps_heading_serial_baudrate': 4800,
                 'gps_heading_serial_port': 'internalGPS',
                 'joystick_device': '/dev/input/js0',
                 'last_auv_config_xml': 'sparus2_configuration.xml',
                 'last_open_project': '',
                 'map_zoom_scale': 20000,
                 'map_latitude': 41.775,
                 'map_longitude': 3.034,
                 'style_north_arrow_color': '000000',
                 'style_scale_bar_color': '000000',
                 'style_all_missions_color': 'ff0000',
                 'style_all_start_end_markers_color': 'cd0000',
                 'timeout': 3600,
                 'usbl_ip': '127.0.0.1',
                 'usbl_own_id': 1,
                 'usbl_port': 9200,
                 'usbl_send_period': 1,
                 'usbl_send_continuous': True,
                 'usbl_target_id': 2,
                 'usbl_imu_heading_enable': False,
                 'usbl_imu_port': 8001,
                 'vessel_name': 'default',
                 'vessel_configuration':{
                     'default': {
                         'gps_offset_heading': 0.0,
                         'gps_offset_x': 4.0,
                         'gps_offset_y': 3.0,
                         'usbl_magnetic_declination': 0.0,
                         'usbl_offset_x': -3.0,
                         'usbl_offset_y': -2.0,
                         'usbl_offset_z': 0.0,
                         'vessel_length': 15.0,
                         'vessel_width': 7.0,
                         'vrp_offset_x': 0.0,
                         'vrp_offset_y': 0.0
                     }
                 },
                 'visibility_north_arrow': 0.0,
                 'visibility_scale_bar': 0.0,
                 'web_server_state': False,
                 'web_server_port': 5000
                 }


class Config:
    def __init__(self):
        self.settings = None
        self.csettings = None
        self.loaded_path = ''

    def load(self):
        """ Create a configuration folder and file if not exist.
            Load a configuration file on settings"""
        home = os.path.expanduser('~')
        iquaview_dir = os.path.join(home, '.iquaview')
        if not os.path.isdir(iquaview_dir):
            # create folder
            os.makedirs(iquaview_dir)

        # todo:ask user for custom configs_path
        auv_configs_path = os.path.dirname(os.path.abspath(__file__))
        auv_configs_path = auv_configs_path + '/auv_configs'
        if not os.path.isdir(home + '/auv_configs'):
            copytree(auv_configs_path, home + '/auv_configs')

        # if app.config not exist or is empty
        if not os.path.isfile(iquaview_dir + "/app.config") or os.stat(iquaview_dir + "/app.config").st_size == 0:
            # create app.config
            f = open(iquaview_dir + "/app.config", "w+")

            # set configs_path on app.config
            DEFAULTCONFIG['configs_path'] = home + '/auv_configs'
            for key, value in DEFAULTCONFIG.items():
                f.write(key + ': ' + str(value) + '\n')
            f.close()

        path = os.path.join(iquaview_dir, "app.config")

        with open(path, 'r') as f:
            self.settings = yaml.safe_load(f)
            if self.settings is None:
                self.settings = {}
                self.csettings = {}
            else:
                for key, value in DEFAULTCONFIG.items():
                    if key in self.settings:
                        pass
                    else:
                        self.settings[key] = value

            self.loaded_path = path

        self.fix_color_formatting()

    def fix_color_formatting(self):
        """
        Fixes color format from a n digit number not longer than 6 to be a '#' + 6 char string (#ABC123)
        """
        color_settings = ['style_north_arrow_color', 'style_scale_bar_color',
                          'style_all_missions_color', 'style_all_start_end_markers_color']

        for entry in color_settings:
            color = self.settings[entry]

            if type(color) == int:  # The color value started with num, convert to 6 char str
                zeros = 6 - len(str(color))  # Number of zeros in front of the color
                color = '#' + '0'*zeros + str(color)
            elif type(color) == str and color[0] != '#':
                color = '#' + color

            self.settings[entry] = color

    def save(self, path=None):
        """
        Save the settings to disk. The last path is used if no path is given.
        :param path:
        :return:
        """
        if not path:
            path = self.loaded_path

        with open(path, 'w') as f:
            yaml.dump(data=self.settings, stream=f, default_flow_style=False)

        logger.debug(self.settings)

    @property
    def settings(self):
        """
        :return: return last saved settings
        :rtype: dict
        """
        return self.__settings

    @settings.setter
    def settings(self, settings):
        """
        Update saved settings dictionary
        :param settings: new settings dict
        :return: dict
        """
        self.__settings = settings

    @property
    def csettings(self):
        """
        :return: return last temporary settings
        :rtype: dict
        """
        return self.__csettings

    @csettings.setter
    def csettings(self, csettings):
        """
        Update temporary settings dictionary
        :param csettings: new csettings dict
        :return: dict
        """
        self.__csettings = csettings
