#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (c) 2019 Iqua Robotics SL - All Rights Reserved
#
# This file is subject to the terms and conditions defined in file
# 'LICENSE.txt', which is part of this source code package.


"""
Message definition with serialization and deserialization.
Message transmitted through modems.
"""

from dataclasses import dataclass
from typing import Dict, Any
import struct
import cola2.comms.altitude as alt
import cola2.comms.elapsed_time as elt


@dataclass
class BasicMessage:
    """Basic message that is sent and received over acoustic communications."""
    mtype: str = 'I'  # modem type('U': USBL, 'G': Girona500, 'S': Sparus)
    time: float = 0.0  # time when the data is obtained
    latitude: float = 0.0  # robot position latitude
    longitude: float = 0.0  # robot position longitude
    depth: float = 0.0  # robot position depth
    heading_accuracy: float = 0.0  # robot yaw heading or USBL measurement accuraccy
    command_error: int = 0  # command from USBL or status_code from robot
    altitude: float = -1.0  # altitude of the robot
    elapsed_time: int = -1  # elapsed time in the robot

    def is_valid(self) -> bool:
        """Check if the message is valid."""
        return self.mtype != 'I'

    def get_navigation_string(self) -> str:
        """Get navigation data as string."""
        return f"{self.latitude:.6f}, {self.longitude:.6f}, {self.depth:.2f}, {self.heading_accuracy:.2f} deg, {self.altitude:.2f}"

    def as_dict(self) -> Dict[str, Any]:
        """Obtain as a dictionary, useful for JSON serializing."""
        mtype = self.mtype
        if isinstance(mtype, bytes):
            mtype = mtype.decode('latin-1')
        return {
            'mtype': mtype,
            'time': self.time,
            'latitude': self.latitude,
            'longitude': self.longitude,
            'depth': self.depth,
            'heading_accuracy': self.heading_accuracy,
            'command_error': self.command_error,
            'altitude': self.altitude,
            'elapsed_time': self.elapsed_time
        }


class BasicMessageSerializer:
    """Class to serialize/deserialize acoustic messages."""

    def __init__(self, fmt):
        """
        Set format of vehicle code (Error Code or Status Code)
        :param fmt: The vehicle code format
        :type fmt: str
        """
        self.FMT = b'<1s 1d 2d 2f 1I 2B'  # 39 bytes
        self.SIZE = struct.calcsize(self.FMT)
        self.max_message_size = 64

    def serialize(self, msg: BasicMessage) -> str:
        """
        Serialize a basic message.

        :param msg: Message to serialize
        :return: Serialized message
        """
        mtype = msg.mtype.encode('latin-1')
        enc_altitude = alt.encode(msg.altitude)
        enc_elapsed_time = elt.encode(msg.elapsed_time)
        return struct.pack(self.FMT, mtype, msg.time, msg.latitude, msg.longitude, msg.depth,
                           msg.heading_accuracy, msg.command_error, enc_altitude, enc_elapsed_time).decode('latin-1')

    def deserialize(self, mser: str) -> BasicMessage:
        """
        Deserialize a basic message.

        :param mser: Serialized message
        :return: Deserialized message
        """
        # Check length
        try:
            if len(mser) == self.SIZE:
                tmp = struct.unpack(self.FMT, mser.encode('latin-1'))
                msg = BasicMessage()
                msg.mtype = tmp[0]
                msg.time = tmp[1]
                msg.latitude = tmp[2]
                msg.longitude = tmp[3]
                msg.depth = tmp[4]
                msg.heading_accuracy = tmp[5]
                msg.command_error = tmp[6]
                msg.altitude = alt.decode(tmp[7])
                msg.elapsed_time = elt.decode(tmp[8])
                return msg

            # invalid length
            line = f"basic_message.py: Message length invalid {len(mser):d} (recv) != {self.SIZE:d} (fmt)"
            print(line)
            return BasicMessage()
        except Exception as e:
            print('basic_message.py: Error deserializing basic message!')
            print(e)
            return BasicMessage()
