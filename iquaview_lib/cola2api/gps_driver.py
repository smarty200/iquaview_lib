#!/usr/bin/env python3

# Copyright (c) 2020 Iqua Robotics SL
#
# Distributed under the terms of the Modified BSD License (BSD 3-clause)
# see 'LICENSE.txt', which is part of this source code package.

"""
GPS NMEA-183 GGA sequence driver.
"""

import threading
import time
import socket
import select
import sys
import logging
from typing import Union
from dataclasses import dataclass
import serial
from PyQt5.QtCore import pyqtSignal, QObject
from iquaview_lib.utils.calcutils import wrap_angle_degrees


logger = logging.getLogger(__name__)

MAX_INCREMENT_NEW_DATA: float = 2.0  # seconds
TIMEOUT_ETHERNET_READ: float = 1.0  # seconds
TIMEOUT_ETHERNET_CONNECTION_FAILED: float = 5.0  # seconds
ETHERNET_READ_SIZE: int = 1000  # bytes


@dataclass
class GPSPosition(object):
    """Position read from a GGA NMEA string."""
    # position
    time: float
    latitude: float
    longitude: float
    altitude: float
    quality: int

    def is_new(self) -> bool:
        """Check if the position data is considered new."""
        t = time.time()
        if t - self.time < MAX_INCREMENT_NEW_DATA:
            return True
        return False

    def get_position_uncertainty(self) -> float:
        """Get position uncertainty depending on fix quality."""
        # fix
        if self.quality == 1:
            return 4.0
        # dgps, pps, rtk, float rtk
        if self.quality in range(2, 6):
            return 1.0
        # other
        return 1e100

    def get_fix_quality_as_string(self) -> str:
        """Get GPS fix quality as a readable string"""
        if self.quality == 1:
            return "GPS fix"
        elif self.quality == 2:
            return "DGPS fix"
        elif self.quality == 3:
            return "PPS fix"
        elif self.quality == 4:
            return "RTK"
        elif self.quality == 5:
            return "Float RTK"
        elif self.quality == 6:
            return "Estimated (dead reckoning)"
        elif self.quality == 7:
            return "Manual input mode"
        elif self.quality == 8:
            return "Simulation mode"
        return "invalid"


@dataclass
class GPSOrientation(object):
    """Position read from a HDT NMEA string."""
    time: float
    orientation_deg: float

    def is_new(self) -> bool:
        """Check if the data is considered new."""
        t = time.time()
        if t - self.time < MAX_INCREMENT_NEW_DATA:
            return True
        return False


@dataclass
class ConfigGPSDriver(object):
    """Configuration options for the GPSDriver class."""
    # serial
    gga_serial_port: Union[str, None] = None
    gga_baud_rate: int = 9600
    hdt_serial_port: Union[str, None] = None
    hdt_baud_rate: int = 9600
    # ethernet
    gga_ip: Union[str, None] = None
    gga_port: int = 4000
    gga_protocol: str = "NONE"
    hdt_ip: Union[str, None] = None
    hdt_port: int = 4000
    hdt_protocol: str = "NONE"
    # offsets
    offset_yaw_deg: float = 0.0  # with respect to front of ship (using 2 GPS antennas)
    declination_deg: float = 0.0  # declination (only for magnetic orientation)


def degree_minute_to_decimal_degree(degree_minute: float) -> float:
    """
    Transform degree minutes values into decimal degrees (i.e., 3830.0 --> 38.5)
    :param degree_minute: position in degree minutes
    :return: return position in decimal degrees
    """
    degrees = int(degree_minute / 100.0)
    minutes = degree_minute - (degrees * 100.0)
    return degrees + minutes / 60.0


class GPSDriver(QObject):
    """Class to handle GPS readings."""

    gpsconnectionfailed = pyqtSignal()
    gpsparsingfailed = pyqtSignal()

    def __init__(self, config: ConfigGPSDriver) -> None:
        """Constructor."""
        # init
        super(GPSDriver, self).__init__()
        self.config = config
        # init variables
        self.gps_position = GPSPosition(time=0.0, latitude=0.0, longitude=0.0, altitude=0.0, quality=-1)
        self.gps_orientation = GPSOrientation(time=0.0, orientation_deg=0.0)
        self.stream_gga: Union[serial.Serial, socket.socket, None] = None
        self.stream_hdt: Union[serial.Serial, socket.socket, None] = None
        self.keepgoing: bool = False
        self.thread: Union[threading.Thread, None] = None

    def __connect_serial_port(self, serial_port: str, baud_rate: int) -> serial.Serial:
        """Connect serial port."""
        # add /dev/ if necessary
        if serial_port.startswith('tty'):
            serial_port = '/dev/' + serial_port
        # connect
        stream = serial.Serial(serial_port, baud_rate, rtscts=True, dsrdtr=True)
        return stream

    def __connect_ethernet(self, addr: str, port: int, protocol: str) -> socket.socket:
        """Connect to TCP address."""
        if protocol == 'TCP':
            return self.__connect_tcp_ip_port(addr, port)
        elif protocol == 'UDP':
            return self.__connect_udp_ip_port(addr, port)
        # error
        line = f"gpsdrv: no protocol specified for ethernet TCP/UDP (provided: '{protocol}')"
        logger.error(line)
        raise Exception(line)

    def __connect_tcp_ip_port(self, addr: str, port: int) -> socket.socket:
        """Connect to TCP address."""
        stream = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        stream.settimeout(2.0)
        stream.connect((addr, port))
        stream.setblocking(False)
        return stream

    def __connect_udp_ip_port(self, addr: str, port: int) -> socket.socket:
        """Connect to UDP address."""
        stream = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        stream.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        stream.settimeout(2.0)
        stream.bind((addr, port))
        return stream

    def connect(self):
        """ Connect by serial or ip to GPS"""
        # GGA error check
        if (self.config.gga_serial_port is not None) and (self.config.gga_ip is not None):
            # GGA over serial and ethernet
            line = "gpsdrv: cannot connect to both serial and ethernet for NMEA GGA"
            logger.error(line)
            raise Exception(line)
        elif (self.config.gga_serial_port is None) and (self.config.gga_ip is None):
            # no GGA
            line = "gpsdrv: no serial or ethernet specified for NMEA GGA"
            logger.error(line)
            raise Exception(line)
        # HDT error check
        if (self.config.hdt_serial_port is not None) and (self.config.hdt_ip is not None):
            # HDT over serial and ethernet
            line = "gpsdrv: cannot connect to both serial and ethernet for NMEA HDT"
            logger.error(line)
            raise Exception(line)
        elif (self.config.hdt_serial_port is None) and (self.config.hdt_ip is None):
            # no HDT
            line = "gpsdrv: no serial or ethernet specified for NMEA HDT"
            logger.error(line)
            raise Exception(line)

        # check grouped
        if (self.config.gga_serial_port is not None) and (self.config.hdt_serial_port is not None):
            # two serial connections, same device?
            if self.config.gga_serial_port == self.config.hdt_serial_port:
                # connect single
                self.stream_gga = self.__connect_serial_port(self.config.gga_serial_port, self.config.gga_baud_rate)
                self.stream_hdt = None
            else:
                # connect both
                self.stream_gga = self.__connect_serial_port(self.config.gga_serial_port, self.config.gga_baud_rate)
                self.stream_hdt = self.__connect_serial_port(self.config.hdt_serial_port, self.config.hdt_baud_rate)
        elif (self.config.gga_ip is not None) and (self.config.hdt_ip is not None):
            # two ethernet connections, same address?
            if (self.config.gga_ip == self.config.hdt_ip) and (self.config.gga_port == self.config.hdt_port):
                # connect single
                self.stream_gga = self.__connect_ethernet(
                    self.config.gga_ip, self.config.gga_port, self.config.gga_protocol)
                self.stream_hdt = None
            else:
                # connect both
                self.stream_gga = self.__connect_ethernet(
                    self.config.gga_ip, self.config.gga_port, self.config.gga_protocol)
                self.stream_hdt = self.__connect_ethernet(
                    self.config.hdt_ip, self.config.hdt_port, self.config.hdt_protocol)
        else:
            # serial + ethernet or vice-versa
            # serial GGA
            if self.config.gga_serial_port is not None:
                self.stream_gga = self.__connect_serial_port(self.config.gga_serial_port, self.config.gga_baud_rate)
            # serial HDT
            if self.config.hdt_serial_port is not None:
                self.stream_hdt = self.__connect_serial_port(self.config.hdt_serial_port, self.config.hdt_baud_rate)
            # ethernet GGA
            if self.config.gga_ip is not None:
                self.stream_gga = self.__connect_ethernet(
                    self.config.gga_ip, self.config.gga_port, self.config.gga_protocol)
            # ethernet HDT
            if self.config.hdt_ip is not None:
                self.stream_hdt = self.__connect_ethernet(
                    self.config.hdt_ip, self.config.hdt_port, self.config.hdt_protocol)

        # start reading thread
        self.keepgoing = True
        self.thread = threading.Thread(target=self.read_gps)
        self.thread.daemon = True
        self.thread.start()

        # logger
        gga_mode = "SERIAL" if isinstance(self.stream_gga, serial.Serial) else "ETHERNET"
        if gga_mode == "ETHERNET":
            gga_mode += f" {self.config.gga_protocol}"
        hdt_mode = "SERIAL" if isinstance(self.stream_hdt, serial.Serial) else "ETHERNET"
        if hdt_mode == "ETHERNET":
            hdt_mode += f" {self.config.hdt_protocol}"
        logger.debug(f"gpsdrv: GGA mode {gga_mode}")
        logger.debug(f"gpsdrv: HDT mode {hdt_mode}")
        logger.debug("gpsdrv: started")

    def read_gps(self) -> None:
        """Parse GPS sequences."""
        while self.keepgoing:
            # read GGA (or both GGA & HDT if single connection)
            try:
                if isinstance(self.stream_gga, serial.Serial):
                    # serial: read directly
                    line = self.stream_gga.readline().decode()
                    logger.debug(f"<<< gpsdrv: serial: {line:s}")
                    if self.is_gga(line):
                        self.parse_xxGGA(line)
                    elif self.stream_hdt is None and self.is_hdt(line):
                        self.parse_xxHDT(line)
                elif isinstance(self.stream_gga, socket.socket):
                    # ethernet: check availability
                    rlist, _, _ = select.select([self.stream_gga], [], [], TIMEOUT_ETHERNET_READ)
                    if rlist:
                        # something to read
                        data = self.stream_gga.recv(ETHERNET_READ_SIZE).decode()
                        lines = data.split('\r\n')
                        for line in lines:
                            logger.debug(f"<<< gpsdrv: ethernet: {line:s}")
                            if self.is_gga(line):
                                self.parse_xxGGA(line)
                            elif self.stream_hdt is None and self.is_hdt(line):
                                self.parse_xxHDT(line)
                # check failed connection
                if self.gps_position.time > 0.0 and (time.time() - self.gps_position.time) > TIMEOUT_ETHERNET_CONNECTION_FAILED:
                    line = "gpsdrv: connection dropped"
                    print(line)
                    logger.error(line)
                    self.keepgoing = False
                    self.gpsconnectionfailed.emit()
            except UnicodeDecodeError:
                pass
            except serial.SerialException as e:
                line = "gpsdrv: error reading serial gps GGA"
                logger.error(e)
                logger.error(line)
                self.keepgoing = False
                self.gpsconnectionfailed.emit()

            # second connection for HDT
            if self.stream_hdt is not None:
                try:
                    if isinstance(self.stream_hdt, serial.Serial):
                        # serial: read directly
                        line = self.stream_hdt.readline().decode()
                        logger.debug(f"<<< gpsdrv: serial: {line:s}")
                        if self.is_hdt(line):
                            self.parse_xxHDT(line)
                    elif isinstance(self.stream_hdt, socket.socket):
                        # ethernet: check availability
                        rlist, _, _ = select.select([self.stream_hdt], [], [], TIMEOUT_ETHERNET_READ)
                        if rlist:
                            # something to read
                            data = self.stream_hdt.recv(ETHERNET_READ_SIZE).decode()
                            lines = data.split('\r\n')
                            for line in lines:
                                logger.debug(f"<<< gpsdrv: ethernet: {line:s}")
                                if self.is_hdt(line):
                                    self.parse_xxHDT(line)
                    # check failed connection
                    if self.gps_orientation.time > 0.0 and (time.time() - self.gps_orientation.time) > TIMEOUT_ETHERNET_CONNECTION_FAILED:
                        line = "gpsdrv: connection dropped"
                        print(line)
                        logger.error(line)
                        self.keepgoing = False
                        self.gpsconnectionfailed.emit()
                except UnicodeDecodeError:
                    pass
                except serial.SerialException as e:
                    line = "gpsdrv: error reading serial gps HDT"
                    logger.error(e)
                    logger.error(line)
                    self.keepgoing = False
                    self.gpsconnectionfailed.emit()
            # time.sleep(0.05)

    def is_gga(self, line: str) -> bool:
        """Check if line corresponds to GGA."""
        if line.startswith('GGA', 3):
            return True
        return False

    def parse_xxGGA(self, line: str) -> None:
        """
        Read xxGGA sequence.
        $xxGGA,time,lat,N/S,lon,E/W,fix,sat,hdop,alt,M,hgeo,M,,*chk
        $xxGGA, 082051.800, 3840.3358, N, 00908.4963, W, 1, 9, 0.86, 2.2, M, 50.7, M,, *46
        :param line: line to parse
        """
        try:
            fields = line.split(',')
            if len(fields) >= 6:
                # GGA, time, lat, N/S, lon, E/W
                self.gps_position.time = time.time()
                self.gps_position.latitude = degree_minute_to_decimal_degree(float(fields[2]))
                if fields[3] == 'S':
                    self.gps_position.latitude *= -1.0
                self.gps_position.longitude = degree_minute_to_decimal_degree(float(fields[4]))
                if fields[5] == 'W':
                    self.gps_position.longitude *= -1.0
            if len(fields) >= 7:
                self.gps_position.quality = int(fields[6])
            if len(fields) >= 10:
                self.gps_position.altitude = float(fields[9])
        except Exception as e:
            logger.error(f"gpsdrv: problem parsing $xxGGA: {e}")
            self.gpsparsingfailed.emit()

    def is_hdt(self, line: str) -> bool:
        """Check if line corresponds to HDT."""
        if line.startswith('HDT', 3):
            return True
        return False

    def parse_xxHDT(self, line: str) -> None:
        """
        Parse xxHDT sentence that contains heading in degrees
            $xxHDT,XX.X,T
        :param line: line to parse
        """
        try:
            fields = line.split(',')
            if len(fields[1]) > 1:
                # assume we are only reading DGPS to apply offset_yaw_deg
                self.gps_orientation.orientation_deg = wrap_angle_degrees(float(fields[1]) - self.config.offset_yaw_deg)
                self.gps_orientation.time = time.time()
                logger.debug('GPS_HDT: {:.3f} {:s} deg'.format(
                    self.gps_orientation.time, fields[1]
                ))
                logger.debug('GPS_HDT_with_offset: {:.3f} {:.3f} deg'.format(
                    self.gps_orientation.time, self.gps_orientation.orientation_deg
                ))
        except Exception as e:
            logger.error(f"gpsdrv: problem parsing $xxHDT: {e}")
            self.gpsparsingfailed.emit()

    def get_position(self) -> GPSPosition:
        """Return last gathered position."""
        return self.gps_position

    def get_orientation(self) -> GPSOrientation:
        """Return last gathered position."""
        return self.gps_orientation

    def close(self) -> None:
        """Close the open connections."""
        # stop loop
        self.keepgoing = False
        # wait for loop thread to join
        try:
            if self.thread is not None:
                self.thread.join(2.0)
        except RuntimeError as e:
            logger.error(str(e.args[0]))
        # close streams
        if self.stream_gga is not None:
            self.stream_gga.close()
        if self.stream_hdt is not None:
            self.stream_hdt.close()


def test() -> None:
    """Test the GPS driver."""
    # input arguments
    if (len(sys.argv) != 3) and (len(sys.argv) != 5):
        print(f"Usage:")
        print(f"  {sys.argv[0]:s} /dev/tty* baudrate [/dev/tty* baudrate]")
        print(f"  {sys.argv[0]:s} /dev/tty* baudrate [ip port]")
        print(f"  {sys.argv[0]:s} ip port [ip port]")
        print(f"  {sys.argv[0]:s} ip port [/dev/tty* baudrate")
        return

    # create config
    config = ConfigGPSDriver()
    if '/' in sys.argv[1]:
        # serial
        config.gga_serial_port = sys.argv[1]
        config.gga_baud_rate = int(sys.argv[2])
        config.hdt_serial_port = config.gga_serial_port
        config.hdt_baud_rate = config.gga_baud_rate
    else:
        # ethernet
        config.gga_ip = sys.argv[1]
        config.gga_port = int(sys.argv[2])
        config.gga_protocol = "TCP"
        config.hdt_ip = config.gga_ip
        config.hdt_port = config.gga_port
        config.hdt_protocol = "TCP"
    # two interfaces
    if len(sys.argv) == 4:
        if '/' in sys.argv[3]:
            # serial
            config.hdt_serial_port = sys.argv[3]
            config.hdt_baud_rate = int(sys.argv[4])
        else:
            # ethernet
            config.hdt_ip = sys.argv[3]
            config.hdt_port = int(sys.argv[4])
            config.hdt_protocol = "TCP"

    # start gps driver
    gps = GPSDriver(config)
    gps.connect()
    # keep asking gps
    while gps.keepgoing:
        time.sleep(0.5)
        print(gps.get_position())
        print(gps.get_orientation())


if __name__ == "__main__":
    test()
