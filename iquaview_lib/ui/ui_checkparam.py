# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_checkparam.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_CheckParameterWidget(object):
    def setupUi(self, CheckParameterWidget):
        CheckParameterWidget.setObjectName("CheckParameterWidget")
        CheckParameterWidget.resize(400, 43)
        self.gridLayout = QtWidgets.QGridLayout(CheckParameterWidget)
        self.gridLayout.setObjectName("gridLayout")
        self.value_label = QtWidgets.QLabel(CheckParameterWidget)
        self.value_label.setObjectName("value_label")
        self.gridLayout.addWidget(self.value_label, 1, 1, 1, 1)
        self.value_lineEdit = QtWidgets.QLineEdit(CheckParameterWidget)
        self.value_lineEdit.setObjectName("value_lineEdit")
        self.gridLayout.addWidget(self.value_lineEdit, 1, 2, 1, 2)
        self.remove_pushButton = QtWidgets.QPushButton(CheckParameterWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.remove_pushButton.sizePolicy().hasHeightForWidth())
        self.remove_pushButton.setSizePolicy(sizePolicy)
        self.remove_pushButton.setText("")
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/resources/mActionDeleteSelected.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.remove_pushButton.setIcon(icon)
        self.remove_pushButton.setFlat(True)
        self.remove_pushButton.setObjectName("remove_pushButton")
        self.gridLayout.addWidget(self.remove_pushButton, 1, 4, 1, 1)

        self.retranslateUi(CheckParameterWidget)
        QtCore.QMetaObject.connectSlotsByName(CheckParameterWidget)

    def retranslateUi(self, CheckParameterWidget):
        _translate = QtCore.QCoreApplication.translate
        CheckParameterWidget.setWindowTitle(_translate("CheckParameterWidget", "CheckParam"))
        self.value_label.setText(_translate("CheckParameterWidget", "Value:"))

from iquaview_lib import resources_rc

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    CheckParameterWidget = QtWidgets.QWidget()
    ui = Ui_CheckParameterWidget()
    ui.setupUi(CheckParameterWidget)
    CheckParameterWidget.show()
    sys.exit(app.exec_())

