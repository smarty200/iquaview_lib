# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_section.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_SectionWidget(object):
    def setupUi(self, SectionWidget):
        SectionWidget.setObjectName("SectionWidget")
        SectionWidget.resize(320, 138)
        self.gridLayout = QtWidgets.QGridLayout(SectionWidget)
        self.gridLayout.setObjectName("gridLayout")
        self.description_lineEdit = QtWidgets.QLineEdit(SectionWidget)
        self.description_lineEdit.setObjectName("description_lineEdit")
        self.gridLayout.addWidget(self.description_lineEdit, 1, 1, 1, 2)
        self.description_label = QtWidgets.QLabel(SectionWidget)
        self.description_label.setObjectName("description_label")
        self.gridLayout.addWidget(self.description_label, 1, 0, 1, 1)
        self.remove_section_pushButton = QtWidgets.QPushButton(SectionWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.remove_section_pushButton.sizePolicy().hasHeightForWidth())
        self.remove_section_pushButton.setSizePolicy(sizePolicy)
        self.remove_section_pushButton.setText("")
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/resources/mActionDeleteSelected.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.remove_section_pushButton.setIcon(icon)
        self.remove_section_pushButton.setFlat(True)
        self.remove_section_pushButton.setObjectName("remove_section_pushButton")
        self.gridLayout.addWidget(self.remove_section_pushButton, 0, 2, 1, 1, QtCore.Qt.AlignRight)
        self.params_groupBox = QtWidgets.QGroupBox(SectionWidget)
        self.params_groupBox.setStyleSheet("QGroupBox {\n"
"    border: 1px solid silver;\n"
"    border-radius: 6px;\n"
"    margin-top: 6px;\n"
"}\n"
"\n"
"QGroupBox::title {\n"
"    subcontrol-origin: margin;\n"
"    left: 7px;\n"
"    padding: 0px 5px 0px 5px;\n"
"}")
        self.params_groupBox.setObjectName("params_groupBox")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.params_groupBox)
        self.verticalLayout.setObjectName("verticalLayout")
        self.params_vboxlayout = QtWidgets.QVBoxLayout()
        self.params_vboxlayout.setObjectName("params_vboxlayout")
        self.verticalLayout.addLayout(self.params_vboxlayout)
        self.add_param_pushButton = QtWidgets.QPushButton(self.params_groupBox)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.add_param_pushButton.sizePolicy().hasHeightForWidth())
        self.add_param_pushButton.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setItalic(True)
        self.add_param_pushButton.setFont(font)
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(":/resources/mActionAdd.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.add_param_pushButton.setIcon(icon1)
        self.add_param_pushButton.setObjectName("add_param_pushButton")
        self.verticalLayout.addWidget(self.add_param_pushButton)
        self.gridLayout.addWidget(self.params_groupBox, 3, 0, 1, 3)

        self.retranslateUi(SectionWidget)
        QtCore.QMetaObject.connectSlotsByName(SectionWidget)
        SectionWidget.setTabOrder(self.description_lineEdit, self.add_param_pushButton)
        SectionWidget.setTabOrder(self.add_param_pushButton, self.remove_section_pushButton)

    def retranslateUi(self, SectionWidget):
        _translate = QtCore.QCoreApplication.translate
        SectionWidget.setWindowTitle(_translate("SectionWidget", "Section"))
        self.description_label.setText(_translate("SectionWidget", "Section description:"))
        self.params_groupBox.setTitle(_translate("SectionWidget", "Parameters"))
        self.add_param_pushButton.setText(_translate("SectionWidget", "New Param"))

from iquaview_lib import resources_rc

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    SectionWidget = QtWidgets.QWidget()
    ui = Ui_SectionWidget()
    ui.setupUi(SectionWidget)
    SectionWidget.show()
    sys.exit(app.exec_())

