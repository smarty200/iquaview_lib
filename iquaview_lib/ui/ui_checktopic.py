# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_checktopic.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_CheckTopicWidget(object):
    def setupUi(self, CheckTopicWidget):
        CheckTopicWidget.setObjectName("CheckTopicWidget")
        CheckTopicWidget.resize(314, 169)
        self.gridLayout = QtWidgets.QGridLayout(CheckTopicWidget)
        self.gridLayout.setObjectName("gridLayout")
        self.description_label = QtWidgets.QLabel(CheckTopicWidget)
        self.description_label.setObjectName("description_label")
        self.gridLayout.addWidget(self.description_label, 1, 0, 1, 1)
        self.fields_groupBox = QtWidgets.QGroupBox(CheckTopicWidget)
        self.fields_groupBox.setStyleSheet("QGroupBox {\n"
"    border: 1px solid silver;\n"
"    border-radius: 6px;\n"
"    margin-top: 6px;\n"
"}\n"
"\n"
"QGroupBox::title {\n"
"    subcontrol-origin: margin;\n"
"    left: 7px;\n"
"    padding: 0px 5px 0px 5px;\n"
"}")
        self.fields_groupBox.setObjectName("fields_groupBox")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.fields_groupBox)
        self.verticalLayout.setObjectName("verticalLayout")
        self.fields_vboxlayout = QtWidgets.QVBoxLayout()
        self.fields_vboxlayout.setObjectName("fields_vboxlayout")
        self.verticalLayout.addLayout(self.fields_vboxlayout)
        self.add_field_pushButton = QtWidgets.QPushButton(self.fields_groupBox)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.add_field_pushButton.sizePolicy().hasHeightForWidth())
        self.add_field_pushButton.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setItalic(True)
        self.add_field_pushButton.setFont(font)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/resources/mActionAdd.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.add_field_pushButton.setIcon(icon)
        self.add_field_pushButton.setObjectName("add_field_pushButton")
        self.verticalLayout.addWidget(self.add_field_pushButton)
        self.gridLayout.addWidget(self.fields_groupBox, 3, 0, 1, 2)
        self.description_lineEdit = QtWidgets.QLineEdit(CheckTopicWidget)
        self.description_lineEdit.setObjectName("description_lineEdit")
        self.gridLayout.addWidget(self.description_lineEdit, 1, 1, 1, 1)
        self.remove_pushButton = QtWidgets.QPushButton(CheckTopicWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.remove_pushButton.sizePolicy().hasHeightForWidth())
        self.remove_pushButton.setSizePolicy(sizePolicy)
        self.remove_pushButton.setText("")
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(":/resources/mActionDeleteSelected.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.remove_pushButton.setIcon(icon1)
        self.remove_pushButton.setFlat(True)
        self.remove_pushButton.setObjectName("remove_pushButton")
        self.gridLayout.addWidget(self.remove_pushButton, 0, 1, 1, 1, QtCore.Qt.AlignRight)
        self.topic_label = QtWidgets.QLabel(CheckTopicWidget)
        self.topic_label.setObjectName("topic_label")
        self.gridLayout.addWidget(self.topic_label, 2, 0, 1, 1)
        self.topic_lineEdit = QtWidgets.QLineEdit(CheckTopicWidget)
        self.topic_lineEdit.setObjectName("topic_lineEdit")
        self.gridLayout.addWidget(self.topic_lineEdit, 2, 1, 1, 1)

        self.retranslateUi(CheckTopicWidget)
        QtCore.QMetaObject.connectSlotsByName(CheckTopicWidget)
        CheckTopicWidget.setTabOrder(self.description_lineEdit, self.topic_lineEdit)
        CheckTopicWidget.setTabOrder(self.topic_lineEdit, self.add_field_pushButton)
        CheckTopicWidget.setTabOrder(self.add_field_pushButton, self.remove_pushButton)

    def retranslateUi(self, CheckTopicWidget):
        _translate = QtCore.QCoreApplication.translate
        CheckTopicWidget.setWindowTitle(_translate("CheckTopicWidget", "CheckTopic"))
        self.description_label.setText(_translate("CheckTopicWidget", "Name:"))
        self.fields_groupBox.setTitle(_translate("CheckTopicWidget", "Fields"))
        self.add_field_pushButton.setText(_translate("CheckTopicWidget", "New Field"))
        self.topic_label.setText(_translate("CheckTopicWidget", "Topic:"))

from iquaview_lib import resources_rc

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    CheckTopicWidget = QtWidgets.QWidget()
    ui = Ui_CheckTopicWidget()
    ui.setupUi(CheckTopicWidget)
    CheckTopicWidget.show()
    sys.exit(app.exec_())

