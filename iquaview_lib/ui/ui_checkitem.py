# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_checkitem.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_CheckItemWidget(object):
    def setupUi(self, CheckItemWidget):
        CheckItemWidget.setObjectName("CheckItemWidget")
        CheckItemWidget.resize(318, 140)
        self.gridLayout = QtWidgets.QGridLayout(CheckItemWidget)
        self.gridLayout.setObjectName("gridLayout")
        self.remove_pushButton = QtWidgets.QPushButton(CheckItemWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.remove_pushButton.sizePolicy().hasHeightForWidth())
        self.remove_pushButton.setSizePolicy(sizePolicy)
        self.remove_pushButton.setText("")
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/resources/mActionDeleteSelected.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.remove_pushButton.setIcon(icon)
        self.remove_pushButton.setFlat(True)
        self.remove_pushButton.setObjectName("remove_pushButton")
        self.gridLayout.addWidget(self.remove_pushButton, 0, 1, 1, 1, QtCore.Qt.AlignRight)
        self.description_label = QtWidgets.QLabel(CheckItemWidget)
        self.description_label.setObjectName("description_label")
        self.gridLayout.addWidget(self.description_label, 1, 0, 1, 1)
        self.description_lineEdit = QtWidgets.QLineEdit(CheckItemWidget)
        self.description_lineEdit.setObjectName("description_lineEdit")
        self.gridLayout.addWidget(self.description_lineEdit, 1, 1, 1, 1)
        self.topics_groupBox = QtWidgets.QGroupBox(CheckItemWidget)
        self.topics_groupBox.setStyleSheet("QGroupBox {\n"
"    border: 1px solid silver;\n"
"    border-radius: 6px;\n"
"    margin-top: 6px;\n"
"}\n"
"\n"
"QGroupBox::title {\n"
"    subcontrol-origin: margin;\n"
"    left: 7px;\n"
"    padding: 0px 5px 0px 5px;\n"
"}")
        self.topics_groupBox.setObjectName("topics_groupBox")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.topics_groupBox)
        self.verticalLayout.setObjectName("verticalLayout")
        self.topics_vboxlayout = QtWidgets.QVBoxLayout()
        self.topics_vboxlayout.setObjectName("topics_vboxlayout")
        self.verticalLayout.addLayout(self.topics_vboxlayout)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.add_topic_pushButton = QtWidgets.QPushButton(self.topics_groupBox)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.add_topic_pushButton.sizePolicy().hasHeightForWidth())
        self.add_topic_pushButton.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setItalic(True)
        self.add_topic_pushButton.setFont(font)
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(":/resources/mActionAdd.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.add_topic_pushButton.setIcon(icon1)
        self.add_topic_pushButton.setObjectName("add_topic_pushButton")
        self.horizontalLayout.addWidget(self.add_topic_pushButton)
        self.add_action_pushButton = QtWidgets.QPushButton(self.topics_groupBox)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.add_action_pushButton.sizePolicy().hasHeightForWidth())
        self.add_action_pushButton.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setItalic(True)
        self.add_action_pushButton.setFont(font)
        self.add_action_pushButton.setIcon(icon1)
        self.add_action_pushButton.setObjectName("add_action_pushButton")
        self.horizontalLayout.addWidget(self.add_action_pushButton)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.gridLayout.addWidget(self.topics_groupBox, 2, 0, 1, 2)

        self.retranslateUi(CheckItemWidget)
        QtCore.QMetaObject.connectSlotsByName(CheckItemWidget)
        CheckItemWidget.setTabOrder(self.description_lineEdit, self.remove_pushButton)

    def retranslateUi(self, CheckItemWidget):
        _translate = QtCore.QCoreApplication.translate
        CheckItemWidget.setWindowTitle(_translate("CheckItemWidget", "CheckItem"))
        self.description_label.setText(_translate("CheckItemWidget", "Description:"))
        self.topics_groupBox.setTitle(_translate("CheckItemWidget", "Topics/Actions"))
        self.add_topic_pushButton.setText(_translate("CheckItemWidget", "New Topic"))
        self.add_action_pushButton.setText(_translate("CheckItemWidget", "New Action"))

from iquaview_lib import resources_rc

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    CheckItemWidget = QtWidgets.QWidget()
    ui = Ui_CheckItemWidget()
    ui.setupUi(CheckItemWidget)
    CheckItemWidget.show()
    sys.exit(app.exec_())

