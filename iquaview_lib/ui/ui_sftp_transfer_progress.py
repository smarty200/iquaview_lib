# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_sftp_transfer_progress.ui'
#
# Created by: PyQt5 UI code generator 5.5.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_transfer_progress(object):
    def setupUi(self, transfer_progress):
        transfer_progress.setObjectName("transfer_progress")
        transfer_progress.resize(502, 106)
        self.verticalLayout = QtWidgets.QVBoxLayout(transfer_progress)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label = QtWidgets.QLabel(transfer_progress)
        self.label.setObjectName("label")
        self.horizontalLayout.addWidget(self.label)
        self.horizontalLayout_5 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_5.setContentsMargins(-1, -1, 0, -1)
        self.horizontalLayout_5.setSpacing(0)
        self.horizontalLayout_5.setObjectName("horizontalLayout_5")
        self.curr_file_label = QtWidgets.QLabel(transfer_progress)
        self.curr_file_label.setObjectName("curr_file_label")
        self.horizontalLayout_5.addWidget(self.curr_file_label)
        self.label_2 = QtWidgets.QLabel(transfer_progress)
        self.label_2.setObjectName("label_2")
        self.horizontalLayout_5.addWidget(self.label_2)
        self.n_files_label = QtWidgets.QLabel(transfer_progress)
        self.n_files_label.setObjectName("n_files_label")
        self.horizontalLayout_5.addWidget(self.n_files_label)
        self.horizontalLayout.addLayout(self.horizontalLayout_5)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.label_3 = QtWidgets.QLabel(transfer_progress)
        self.label_3.setObjectName("label_3")
        self.horizontalLayout.addWidget(self.label_3)
        self.horizontalLayout_6 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_6.setContentsMargins(-1, -1, 0, -1)
        self.horizontalLayout_6.setSpacing(0)
        self.horizontalLayout_6.setObjectName("horizontalLayout_6")
        self.curr_kbytes_label = QtWidgets.QLabel(transfer_progress)
        self.curr_kbytes_label.setObjectName("curr_kbytes_label")
        self.horizontalLayout_6.addWidget(self.curr_kbytes_label)
        self.label_4 = QtWidgets.QLabel(transfer_progress)
        self.label_4.setObjectName("label_4")
        self.horizontalLayout_6.addWidget(self.label_4)
        self.total_kbytes_label = QtWidgets.QLabel(transfer_progress)
        self.total_kbytes_label.setObjectName("total_kbytes_label")
        self.horizontalLayout_6.addWidget(self.total_kbytes_label)
        self.horizontalLayout.addLayout(self.horizontalLayout_6)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.progress_bar = QtWidgets.QProgressBar(transfer_progress)
        self.progress_bar.setProperty("value", 24)
        self.progress_bar.setObjectName("progress_bar")
        self.verticalLayout.addWidget(self.progress_bar)
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_4.addItem(spacerItem1)
        self.cancel_button = QtWidgets.QPushButton(transfer_progress)
        self.cancel_button.setObjectName("cancel_button")
        self.horizontalLayout_4.addWidget(self.cancel_button)
        self.verticalLayout.addLayout(self.horizontalLayout_4)

        self.retranslateUi(transfer_progress)
        QtCore.QMetaObject.connectSlotsByName(transfer_progress)

    def retranslateUi(self, transfer_progress):
        _translate = QtCore.QCoreApplication.translate
        transfer_progress.setWindowTitle(_translate("transfer_progress", "Transfer Progress"))
        self.label.setText(_translate("transfer_progress", "Transfering files:"))
        self.curr_file_label.setText(_translate("transfer_progress", "X"))
        self.label_2.setText(_translate("transfer_progress", "/"))
        self.n_files_label.setText(_translate("transfer_progress", "Y"))
        self.label_3.setText(_translate("transfer_progress", "Kbytes transfered:"))
        self.curr_kbytes_label.setText(_translate("transfer_progress", "X"))
        self.label_4.setText(_translate("transfer_progress", "/"))
        self.total_kbytes_label.setText(_translate("transfer_progress", "Y"))
        self.cancel_button.setText(_translate("transfer_progress", "Cancel"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    transfer_progress = QtWidgets.QDialog()
    ui = Ui_transfer_progress()
    ui.setupUi(transfer_progress)
    transfer_progress.show()
    sys.exit(app.exec_())

