# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_checkaction.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_CheckActionWidget(object):
    def setupUi(self, CheckActionWidget):
        CheckActionWidget.setObjectName("CheckActionWidget")
        CheckActionWidget.resize(400, 169)
        self.gridLayout = QtWidgets.QGridLayout(CheckActionWidget)
        self.gridLayout.setObjectName("gridLayout")
        self.remove_pushButton = QtWidgets.QPushButton(CheckActionWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.remove_pushButton.sizePolicy().hasHeightForWidth())
        self.remove_pushButton.setSizePolicy(sizePolicy)
        self.remove_pushButton.setText("")
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/resources/mActionDeleteSelected.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.remove_pushButton.setIcon(icon)
        self.remove_pushButton.setFlat(True)
        self.remove_pushButton.setObjectName("remove_pushButton")
        self.gridLayout.addWidget(self.remove_pushButton, 0, 1, 1, 1, QtCore.Qt.AlignRight)
        self.name_label = QtWidgets.QLabel(CheckActionWidget)
        self.name_label.setObjectName("name_label")
        self.gridLayout.addWidget(self.name_label, 1, 0, 1, 1)
        self.name_lineEdit = QtWidgets.QLineEdit(CheckActionWidget)
        self.name_lineEdit.setObjectName("name_lineEdit")
        self.gridLayout.addWidget(self.name_lineEdit, 1, 1, 1, 1)
        self.service_label = QtWidgets.QLabel(CheckActionWidget)
        self.service_label.setObjectName("service_label")
        self.gridLayout.addWidget(self.service_label, 2, 0, 1, 1)
        self.service_lineEdit = QtWidgets.QLineEdit(CheckActionWidget)
        self.service_lineEdit.setObjectName("service_lineEdit")
        self.gridLayout.addWidget(self.service_lineEdit, 2, 1, 1, 1)
        self.parameters_groupBox = QtWidgets.QGroupBox(CheckActionWidget)
        self.parameters_groupBox.setStyleSheet("QGroupBox {\n"
"    border: 1px solid silver;\n"
"    border-radius: 6px;\n"
"    margin-top: 6px;\n"
"}\n"
"\n"
"QGroupBox::title {\n"
"    subcontrol-origin: margin;\n"
"    left: 7px;\n"
"    padding: 0px 5px 0px 5px;\n"
"}")
        self.parameters_groupBox.setObjectName("parameters_groupBox")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.parameters_groupBox)
        self.verticalLayout.setObjectName("verticalLayout")
        self.parameters_vboxlayout = QtWidgets.QVBoxLayout()
        self.parameters_vboxlayout.setObjectName("parameters_vboxlayout")
        self.verticalLayout.addLayout(self.parameters_vboxlayout)
        self.add_param_pushButton = QtWidgets.QPushButton(self.parameters_groupBox)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.add_param_pushButton.sizePolicy().hasHeightForWidth())
        self.add_param_pushButton.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setItalic(True)
        self.add_param_pushButton.setFont(font)
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(":/resources/mActionAdd.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.add_param_pushButton.setIcon(icon1)
        self.add_param_pushButton.setObjectName("add_param_pushButton")
        self.verticalLayout.addWidget(self.add_param_pushButton)
        self.gridLayout.addWidget(self.parameters_groupBox, 3, 0, 1, 2)

        self.retranslateUi(CheckActionWidget)
        QtCore.QMetaObject.connectSlotsByName(CheckActionWidget)
        CheckActionWidget.setTabOrder(self.name_lineEdit, self.service_lineEdit)
        CheckActionWidget.setTabOrder(self.service_lineEdit, self.add_param_pushButton)
        CheckActionWidget.setTabOrder(self.add_param_pushButton, self.remove_pushButton)

    def retranslateUi(self, CheckActionWidget):
        _translate = QtCore.QCoreApplication.translate
        CheckActionWidget.setWindowTitle(_translate("CheckActionWidget", "CheckAction"))
        self.name_label.setText(_translate("CheckActionWidget", "Name:"))
        self.service_label.setText(_translate("CheckActionWidget", "Service:"))
        self.parameters_groupBox.setTitle(_translate("CheckActionWidget", "Parameters"))
        self.add_param_pushButton.setText(_translate("CheckActionWidget", "New Parameter"))

from iquaview_lib import resources_rc

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    CheckActionWidget = QtWidgets.QWidget()
    ui = Ui_CheckActionWidget()
    ui.setupUi(CheckActionWidget)
    CheckActionWidget.show()
    sys.exit(app.exec_())

