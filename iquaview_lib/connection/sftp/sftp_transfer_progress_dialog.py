"""
Copyright (c) 2020 Iqua Robotics SL

# Distributed under the terms of the Modified BSD License (BSD 3-clause)
# see 'LICENSE.txt', which is part of this source code package.
"""
from PyQt5.QtCore import pyqtSignal, Qt
from PyQt5.QtWidgets import QDialog

from iquaview_lib.ui.ui_sftp_transfer_progress import Ui_transfer_progress


class SFTPTransferProgressDialog(QDialog, Ui_transfer_progress):
    cancel_transfer_signal = pyqtSignal()

    def __init__(self, n_files, parent=None):
        super(SFTPTransferProgressDialog, self).__init__(parent)
        self.setupUi(self)
        self.progress_bar.setValue(0)
        self.curr_file_label.setText("1")
        self.n_files_label.setText(str(n_files))
        self.n_files = n_files
        self.cancel_button.clicked.connect(self.close)

    def update_values(self, transferred_bytes, total_bytes):
        total = total_bytes / 1000
        transferred = transferred_bytes / 1000
        progress = 100 * transferred / total
        self.curr_kbytes_label.setText("{:.0F}".format(transferred))
        self.total_kbytes_label.setText("{:.0F}".format(total))
        self.progress_bar.setValue(progress)
        if progress == 100:
            curr_file = int(self.curr_file_label.text())
            if curr_file < self.n_files:
                curr_file += 1
                self.curr_file_label.setText(str(curr_file))

    def keyPressEvent(self, e) -> None:
        """ Override event handler keyPressEvent"""
        if e.key() == Qt.Key_Escape:
            self.cancel_transfer_signal.emit()
            super(SFTPTransferProgressDialog, self).keyPressEvent(e)

    def closeEvent(self, event):
        """ Overrides closeEvent"""
        super(SFTPTransferProgressDialog, self).closeEvent(event)
        self.cancel_transfer_signal.emit()

    def close(self) -> bool:
        self.hide()
        super(SFTPTransferProgressDialog, self).close()
