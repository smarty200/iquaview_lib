"""
Copyright (c) 2020 Iqua Robotics SL

# Distributed under the terms of the Modified BSD License (BSD 3-clause)
# see 'LICENSE.txt', which is part of this source code package.
"""

from PyQt5.QtGui import QStandardItemModel


class SFTPModel(QStandardItemModel):
    def __init__(self, file_icon=None, folder_icon=None):
        """
        Init of the object SFTPModel
        :param file_icon: icon for file rows
        :type file_icon: QIcon
        :param folder_icon: icon for folder rows
        :type folder_icon: QIcon
        """
        super(SFTPModel, self).__init__()
        self.row_list = list()
        self.file_icon = file_icon
        self.folder_icon = folder_icon

    def add_row(self, row):
        """
        Add row to the model
        :param row: row to be added
        :type row: list
        """
        self.row_list.append(row)
        self.appendRow(row)

    def remove_all_rows(self):
        """
        Removes all rows from the model
        """
        self.row_list = list()
        self.removeRows(0, self.rowCount())

    def get_folder(self, name):
        """
        Returns the folder specified by name
        :param name: name of the folder
        :type name: str
        :return: folder item
        :rtype: SFTPFolder
        """
        for r in self.row_list:
            if name == r.text():
                return r
        return None

    def get_file_rows(self):
        """
        Returns the rows that contain files
        :return: list files
        :rtype: list
        """
        files = list()
        for r in self.row_list:
            if r[1].icon().name() == self.file_icon.name():
                files.append(r)
        return files

    def take_file_rows(self):
        """
        Returns the rows that contain files and removes them from the model
        :return: list of files
        :rtype: list
        """
        files = list()
        r = 0
        while r < self.rowCount():
            row = self.row_list[r]
            if type(row) == list:
                if row[1].icon().name() == self.file_icon.name():
                    self.row_list.remove(row)
                    files.append(self.takeRow(r))
                    r = 0
                else:
                    r += 1
            else:
                if row.icon().name() == self.folder_icon.name():
                    self.row_list.remove(row)
                    files.append(self.takeRow(r))
                    r = 0
                else:
                    r += 1
        return files

    def take_folder_rows(self):
        """
        Returns the rows that contain folders and removes them from the model
        :return: list of folders
        :rtype: list
        """
        folders = list()
        r = 0
        while r < self.rowCount():
            row = self.row_list[r]
            if type(row) == list:
                if row[1].icon().name() == self.folder_icon.name():
                    self.row_list.remove(row)
                    folders.append(self.takeRow(r))
                    r = 0
                else:
                    r += 1
            else:
                if row.icon().name() == self.folder_icon.name():
                    self.row_list.remove(row)
                    folders.append(self.takeRow(r))
                    r = 0
                else:
                    r += 1
        return folders
