version_info = (20, 10, 6)

__version__ = '.'.join(map(str, version_info))
