"""
Copyright (c) 2020 Iqua Robotics SL

# Distributed under the terms of the Modified BSD License (BSD 3-clause)
# see 'LICENSE.txt', which is part of this source code package.
"""


"""
 Helper classes to read the xml structure associated to vehicle info in the AUV config file
"""

from iquaview_lib.xmlconfighandler.auvconfigcheckxml import ConfigCheckXML
from iquaview_lib.xmlconfighandler.xmlconfigparser import XMLConfigParser


class VehicleInfoHandler(object):
    def __init__(self, config):
        """
        Constructor
        :param config: configuration
        :type config: Config
        """
        config_check_xml = ConfigCheckXML(config)
        if not config_check_xml.exists():
            config_check_xml.exec_()

        # last auv config xml
        self.configParser = XMLConfigParser(config.csettings['configs_path']
                                            + '/'
                                            + config.csettings['last_auv_config_xml'])

    def read_configuration(self):
        """get Vehicle Info"""
        xml_vehicle_info = self.configParser.first_match(self.configParser.root, "vehicle_info")

        return xml_vehicle_info

    def write(self):
        """write vehicle info on XML"""
        self.configParser.write()
