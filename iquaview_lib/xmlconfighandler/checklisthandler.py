"""
Copyright (c) 2020 Iqua Robotics SL

# Distributed under the terms of the Modified BSD License (BSD 3-clause)
# see 'LICENSE.txt', which is part of this source code package.
"""


"""
 Helper classes to read the xml structure associated to check lists in the AUV config file
"""

from iquaview_lib.xmlconfighandler.xmlconfigparser import XMLConfigParser


class CheckListHandler(object):
    def __init__(self, config):
        """
        Constructor
        :param config: configuration
        :type config: Config
        """
        self.filename = config.csettings['configs_path'] + '/' + config.csettings['last_auv_config_xml']
        self.configParser = XMLConfigParser(self.filename)

    def get_check_lists(self):
        """
        Get check_lists
        :return: return all lists from check_lists
        :rtype: list
        """
        chk_lists = self.configParser.first_match(self.configParser.root, "check_lists")
        # all check_list in check_lists
        lists = self.configParser.all_matches(chk_lists, "check_list")
        return lists

    def get_items_from_checklist(self, checklist_name):
        """
        Get all items from a check_list with name 'checklist_name'
        :param checklist_name: name of the check_list
        :type checklist_name: str
        :return: return a list of items from a check_list with name 'checklist_name'
        :rtype: list
        """
        chk_lists = self.configParser.first_match(self.configParser.root, "check_lists")
        # get specific check_list by attribute chk_name
        xml_check_list = self.configParser.first_match(chk_lists, "check_list[@id='" + checklist_name + "']")
        # all check_list items
        check_items = self.configParser.all_matches(xml_check_list, "check_item")

        return check_items

    def get_description_from_item(self, item):
        """ Get description field from 'item'
        :param item: item is a check_item from xml structure configuration
        :return: return description field from 'item'
        """
        description = self.configParser.first_match(item, "description")
        return description

    def get_check_topics_from_item(self, item):
        """
        Get check_topics from 'item'
        :param item: item is a check_item from xml structure configuration
        :return: return a list of check_topics from 'item'
        """
        check_topics = self.configParser.all_matches(item, "check_topic")
        return check_topics

    def get_check_actions_from_item(self, item):
        """
        Get check_actions from 'item'
        :param item: item is a check_item from xml structure configuration
        :return: return a list of check_actions from 'item'
        """
        check_actions = self.configParser.all_matches(item, "check_action")
        return check_actions
