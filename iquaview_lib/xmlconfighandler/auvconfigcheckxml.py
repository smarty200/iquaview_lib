"""
Copyright (c) 2020 Iqua Robotics SL

# Distributed under the terms of the Modified BSD License (BSD 3-clause)
# see 'LICENSE.txt', which is part of this source code package.
"""


"""
 Helper classes to get a valid xml file
"""

import os.path
from PyQt5.QtWidgets import QDialog, QMessageBox, QFileDialog
from PyQt5.QtCore import QFileInfo
from iquaview_lib.ui.ui_auvconfigxml import Ui_AUVconfigDialog


class ConfigCheckXML(QDialog, Ui_AUVconfigDialog):
    def __init__(self, config):
        """
        Constructor
        :param config: configuration
        :type config: Config
        """
        super(ConfigCheckXML, self).__init__()
        self.setupUi(self)
        self.config = config
        if config.csettings['configs_path'] is not None and config.csettings['last_auv_config_xml'] is not None:
            self.config_filename = config.csettings['configs_path'] + '/' + config.csettings['last_auv_config_xml']
        else:
            self.config_filename = ""
        self.auv_config_lineEdit.setText(config.csettings['last_auv_config_xml'])

        self.auv_config_pushButton.clicked.connect(self.load_auv_config)
        self.buttonBox.accepted.connect(self.on_accept)

    def exists(self):
        """Check if config file exists."""
        exists = os.path.exists(self.config_filename)
        return exists

    def load_auv_config(self):
        """ Open dialog to load an auv config file."""
        configuration_filename, __ = QFileDialog.getOpenFileName(self, 'AUV configuration',
                                                                 self.config.csettings['last_auv_config_xml'],
                                                                 "AUV configuration(*.xml) ;; All files (*.*)")
        if configuration_filename:
            file_info = QFileInfo(configuration_filename)
            filename = file_info.fileName()

            self.auv_config_lineEdit.setText(str(filename))

    def on_accept(self):
        """ On accept config check xml"""
        exists = os.path.exists(self.config.csettings['configs_path'] + '/' + self.auv_config_lineEdit.text())
        if exists:

            # update config
            self.config.settings['last_auv_config_xml'] = self.auv_config_lineEdit.text()
            self.config.csettings['last_auv_config_xml'] = self.config.settings['last_auv_config_xml']

            self.config.save()
            self.accept()

        else:
            QMessageBox.critical(self.parent(),
                                 "AUV configuration XML Error",
                                 "AUV configuration: " + self.auv_config_lineEdit.text() + " no exist.",
                                 QMessageBox.Close)
