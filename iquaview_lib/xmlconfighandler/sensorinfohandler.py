# -*- coding: utf-8 -*-
"""
Copyright (c) 2020 Iqua Robotics SL

# Distributed under the terms of the Modified BSD License (BSD 3-clause)
# see 'LICENSE.txt', which is part of this source code package.
"""

"""
 Helper classes to read the xml structure associated to sensor info in the AUV config file
"""

from iquaview_lib.xmlconfighandler.auvconfigcheckxml import ConfigCheckXML
from iquaview_lib.xmlconfighandler.xmlconfigparser import XMLConfigParser


class SensorInfoHandler(object):
    def __init__(self, config_filename):
        """
        Constructor
        :param config_filename: configuration
        :type config_filename: str
        """

        # last auv config xml
        self.configParser = XMLConfigParser(config_filename)

    def read_configuration(self):
        """get Sensor Info"""
        xml_sensor_info = self.configParser.first_match(self.configParser.root, "sensor_info")

        return xml_sensor_info

    def write(self):
        """write sensor info on XML"""
        self.configParser.write()
