# -*- coding: utf-8 -*-
"""
Copyright (c) 2020 Iqua Robotics SL

# Distributed under the terms of the Modified BSD License (BSD 3-clause)
# see 'LICENSE.txt', which is part of this source code package.
"""

"""
 Class to read and parse diagnostics agg message
"""


class DiagnosticsAggParser:
    def __init__(self, message):
        """
        Constructor
        :param message: diagnostics agg message
        """
        self.message = message

    def get_value(self, name, key):
        """
        Return value from a message by name and key
        :param name: category in diagnostics aggregator
        :type name: str
        :param key: key 
        :type key: str
        :return: value of element
        """
        #TODO: STALE 3 . NO PUBLISHED
        value = None
        if self.message is not None:
            status = self.message.get("status")
            if status is not None:
                for item in status:
                    if item.get("name") == name:
                        for element in item.get("values"):
                            if element.get("key") == key:
                                value = element.get("value")
        return value
