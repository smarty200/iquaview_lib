# -*- coding: utf-8 -*-
"""
Copyright (c) 2020 Iqua Robotics SL

# Distributed under the terms of the Modified BSD License (BSD 3-clause)
# see 'LICENSE.txt', which is part of this source code package.
"""

"""
 Class to read and store the xml structure associated to the sensor_info tag in the config file
"""

from iquaview_lib.xmlconfighandler.sensorinfohandler import SensorInfoHandler


class SensorInfo:
    def __init__(self, config):
        """
        Constructor
        :param config: configuration
        """
        self.config = config

        self.sensor_ip = None
        self.sensor_name = None
        self.sensor_node = None

        # read XML
        self.read_xml()

    def read_xml(self):
        """

        read the last auv configuration xml loaded, and save the sensor information
        """
        vi_reader = SensorInfoHandler(self.config)
        xml_sensor_info = vi_reader.read_configuration()

        for item in xml_sensor_info:
            if item.tag == 'sensor_ip':
                self.sensor_ip = item.text
            elif item.tag == 'sensor_name':
                self.sensor_name = item.text
            elif item.tag == 'sensor_node':
                self.sensor_node = item.text

    def get_sensor_ip(self):
        """
        :return: sensor ip
        :rtype: str
        """
        return self.sensor_ip

    def get_sensor_name(self):
        """
        :return: sensor name
        :rtype: str
        """
        return self.sensor_name

    def get_sensor_node(self):
        """
        :return: sensor name
        :rtype: str
        """
        return self.sensor_node

    def set_sensor_ip(self, sensor_ip):
        """
        Set sensor_ip
        :param sensor_ip: sensor ip address
        :type sensor_ip: str
        """
        self.sensor_ip = sensor_ip

    def set_sensor_name(self, sensor_name):
        """
        Set sensor name
        :param sensor_name:  sensor name
        :type sensor_name: str
        """
        self.sensor_name = sensor_name

    def set_sensor_node(self, sensor_node):
        """
        Set sensor name
        :param sensor_node:  sensor name
        :type sensor_node: str
        """
        self.sensor_node = sensor_node

    def save(self):
        """

        save the sensor info inside the last auv configuration xml
        """
        # last auv config xml
        si_handler = SensorInfoHandler(self.config)
        xml_sensor_info = si_handler.read_configuration()

        for item in xml_sensor_info:
            if item.tag == 'sensor_ip':
                item.text = str(self.sensor_ip)
            elif item.tag == 'sensor_name':
                item.text = str(self.sensor_name)
            elif item.tag == 'sensor_node':
                item.text = str(self.sensor_node)

        si_handler.write()
