"""
Copyright (c) 2020 Iqua Robotics SL

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
"""

"""
    Base class for a iquaview plugin
"""
import logging

from PyQt5.QtCore import QObject

logger = logging.getLogger(__name__)


class PluginBase(QObject):

    def __init__(self, module_name="empty", parent=None):
        super(QObject, self).__init__(parent)
        self.module_name = module_name
        self.module_found = False

    @staticmethod
    def version():
        """Return the version of the plugin"""
        pass

    @property
    def module_name(self):
        """
        :return: return module name
        :rtype: str
        """
        return self.__module_name

    @module_name.setter
    def module_name(self, module_name):
        """
        Set module name
        :param module_name: new module name
        :type module_name: str
        """
        self.__module_name = module_name

    def is_module_found(self):
        """ Return True if module is found in system, otherwise False"""
        return self.module_found

    def disconnect_module(self):
        """ Disconnect module"""
        pass

    def remove(self):
        """ Remove module"""
        pass
