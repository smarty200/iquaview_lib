"""
Copyright (c) 2020 Iqua Robotics SL

# Distributed under the terms of the Modified BSD License (BSD 3-clause)
# see 'LICENSE.txt', which is part of this source code package.
"""


"""
 Utils to perform computations with points and angles
"""

import math


def wrap_angle(angle):
    """
    Wraps any angle (radians) over π to an angle between -π and π
    :param angle: angle to wrap
    :return: angle wraped between -π and π
    :rtype: float
    """
    return angle + (2.0 * math.pi * math.floor((math.pi - angle) / (2.0 * math.pi)))


def wrap_angle_degrees(angle_deg):
    """
    Wraps any angle (degrees) over 180 to an angle between -180 and +180
    :param angle_deg: angle to wrap
    :return: angle wraped between -180 and +180
    :rtype: float
    """
    return angle_deg + (360.0 * math.floor((180.0 - angle_deg) / 360.0))


def get_angle_of_line_between_two_points(p1_x, p1_y, p2_x, p2_y, angle_unit="degrees"):
    """
    Return the angle of the line represents by two points : p1 and p2

    :param p1_x: The first point x
    :param p1_y: The first point y
    :param p2_x: The second point x
    :param p2_y: The second point y
    :param angle_unit: desired angle units
    :type p1_x: float
    :type p1_y: float
    :type p2_x: float
    :type p2_y: float
    :type angle_unit: str
    :return: Return the angle (degree by default)
    :rtype: float
    """
    x_diff = p2_x - p1_x
    y_diff = p2_y - p1_y

    if angle_unit == "radians":
        return math.atan2(y_diff, x_diff)
    else:
        return math.degrees(math.atan2(y_diff, x_diff))


def calc_slope(p1_x, p1_y, p2_x, p2_y):
    """
    Return the slope of the line represents by two points : p1 and p2

    :param p1_x: The first point x
    :param p1_y: The first point y
    :param p2_x: The second point x
    :param p2_y: The second point y
    :type p1_x: float
    :type p1_y: float
    :type p2_x: float
    :type p2_y: float
    :return: Return the slope, can be infinity
    :rtype: float
    """

    num = p1_y - p2_y
    den = p1_x - p2_x

    if num == 0:
        return 0.0
    # Avoid division by zero
    elif den == 0:
        # Depending on num, result can be inf or -inf
        if num > 0:
            return -math.inf
        else:
            return math.inf
    else:
        return num / den


def calc_is_collinear(p0_x, p0_y, p1_x, p1_y, p2_x, p2_y):
    """
    Test if point p2 is on left/on/right of the line made by p0 and p1
    :param p0_x: The first point x
    :param p0_y: The first point y
    :type p0_x: float
    :type p0_y: float
    :param p1_x: The second point x
    :param p1_y: The second point y
    :type p1_x: float
    :type p1_y: float
    :param p2_x: point to test x
    :param p2_y: point to test y
    :type p2_x: float
    :type p2_y: float
    :return: 1 left, 0 collinear, -1 right
    :rtype: int
    """

    sens = ((p1_x - p0_x) * (p2_y - p0_y) - (p1_y - p0_y) * (p2_x - p0_x))

    if sens > 0:
        return -1
    elif sens < 0:
        return 1
    else:
        return 0


def calc_middle_point(p1_x, p1_y, p2_x, p2_y):
    """
    Find the middle point between two points
    :param p1_x: The first point x
    :param p1_y: The first point y
    :param p2_x: The second point x
    :param p2_y: The second point y
    :type p1_x: float
    :type p1_y: float
    :type p2_x: float
    :type p2_y: float
    :return: Middle point found
    :rtype: [float, float]
    """

    return (p1_x + p2_x) / 2.0, (p1_y + p2_y) / 2.0


# from cadtools (c) Stefan ZIegler
def calc_parallel_segment(p1_x, p1_y, p2_x, p2_y, dist):
    """
    Find the points that form a parallel segment to the segment formed by p1 and p2 at a distance of dist
    (to the left if dist > 0, otherwise to the right)
    :param p1_x: The first point x
    :param p1_y: The first point y
    :param p2_x: The second point x
    :param p2_y: The second point y
    :type p1_x: float
    :type p1_y: float
    :type p2_x: float
    :type p2_y: float
    :param dist: distance to the segment of the two points
    :return: two points that form a segment parallel to the initial points [p3_x, p3_y, p4_x, p4_y]
    :rtype: [float, float, float, float]
    """
    if dist == 0:
        return p1_x, p1_y, p2_x, p2_y

    # dist between the two initial points
    dn = distance_plane(p1_x, p1_y, p2_x, p2_y)

    # find the new points
    p3_x = p1_x + dist * (p1_y - p2_y) / dn
    p3_y = p1_y - dist * (p1_x - p2_x) / dn

    p4_x = p2_x + dist * (p1_y - p2_y) / dn
    p4_y = p2_y - dist * (p1_x - p2_x) / dn

    return p3_x, p3_y, p4_x, p4_y


def distance_ellipsoid(p1_x, p1_y, p2_x, p2_y):
    """
    Finds the distance between two points in the globe using WGS84 CRS and an ellipsoid approximation
    :param p1_x: The first point x
    :param p1_y: The first point y
    :param p2_x: The second point x
    :param p2_y: The second point y
    :type p1_x: float
    :type p1_y: float
    :type p2_x: float
    :type p2_y: float
    :return: distance between the two points in meters
    :rtype: float
    """
    # Assumes points are WGS 84 lat/long
    # Returns great circle distance in meters
    radius = 6378137  # meters
    flattening = 1 / 298.257223563

    # Convert to radians with reduced latitudes to compensate
    # for flattening of the earth as in Lambert's formula
    start_lon = p1_x * math.pi / 180
    start_lat = math.atan2((1 - flattening) * math.sin(p1_y * math.pi / 180), math.cos(p1_y * math.pi / 180))
    end_lon = p2_x * math.pi / 180
    end_lat = math.atan2((1 - flattening) * math.sin(p2_y * math.pi / 180), math.cos(p2_y * math.pi / 180))

    # Haversine formula
    arc_distance = (math.sin((end_lat - start_lat) / 2) ** 2) + \
                   (math.cos(start_lat) * math.cos(end_lat) * (math.sin((end_lon - start_lon) / 2) ** 2))

    return 2 * radius * math.atan2(math.sqrt(arc_distance), math.sqrt(1 - arc_distance))


# http://www.movable-type.co.uk/scripts/latlong.html
def bearing(p1_x, p1_y, p2_x, p2_y):
    """
    Finds the bearing between two points in the globe using WGS84 CRS
    :param p1_x: starting point x
    :param p1_y: starting point y
    :param p2_x: end point x
    :param p2_y: end point y
    :type p1_x: float
    :type p1_y: float
    :type p2_x: float
    :type p2_y: float
    :return: bearing between the two points in degrees
    :rtype: float
    """
    # Assumes points are WGS 84 lat/long

    start_lon = p1_x * math.pi / 180
    start_lat = p1_y * math.pi / 180
    end_lon = p2_x * math.pi / 180
    end_lat = p2_y * math.pi / 180

    return math.atan2(math.sin(end_lon - start_lon) * math.cos(end_lat),
                      (math.cos(start_lat) * math.sin(end_lat))
                      - (math.sin(start_lat) * math.cos(end_lat) * math.cos(end_lon - start_lon))) * 180 / math.pi


# http://www.movable-type.co.uk/scripts/latlong.html
def endpoint_sphere(start_x, start_y, dist, degrees_bearing):
    """
    Finds an end point given a start point, a distance and an angle in the globe using sphere approximation.
    :param start_x: start point x(WGS84 CRS)
    :type start_x: float
    :param start_y: start point y (WGS84 CRS)
    :type start_y: float
    :param dist: distance to the end point (meters)
    :type dist: float
    :param degrees_bearing: bearing between start point and end point (degrees)
    :type degrees_bearing: float
    :return: end point
    :rtype: [float, float]
    """

    radius = 6378137.0  # meters

    start_lon = start_x * math.pi / 180
    start_lat = start_y * math.pi / 180
    bearing = degrees_bearing * math.pi / 180

    end_lat = math.asin((math.sin(start_lat) * math.cos(dist / radius)) +
                        (math.cos(start_lat) * math.sin(dist / radius) * math.cos(bearing)))
    end_lon = start_lon + math.atan2(math.sin(bearing) * math.sin(dist / radius) * math.cos(start_lat),
                                     math.cos(dist / radius) - (math.sin(start_lat) * math.sin(end_lat)))

    return end_lon * 180 / math.pi, end_lat * 180 / math.pi


# https://www.movable-type.co.uk/scripts/latlong-vincenty.html
def endpoint_ellipsoid(start_x, start_y, dist, degrees_bearing):
    """
    Finds an end point given a start point, a distance and an angle in the globe using ellipsoid approximation.
    :param start_x: start point x(WGS84 CRS)
    :type start_x: float
    :param start_y: start point y (WGS84 CRS)
    :type start_y: float
    :param dist: distance to the end point (meters)
    :type dist: float
    :param degrees_bearing: bearing between start point and end point (degrees)
    :type degrees_bearing: float
    :return: end point
    :rtype: [float, float]
    """

    start_lon = start_x * math.pi / 180
    start_lat = start_y * math.pi / 180
    bearing = degrees_bearing * math.pi / 180
    d = dist

    a = 6378137
    f = 0.003352813
    b = 6356752.3142

    sin_angle1 = math.sin(bearing)
    cos_angle1 = math.cos(bearing)

    tan_U1 = (1-f) * math.tan(start_lat)
    cos_U1 = 1 / math.sqrt((1+ tan_U1*tan_U1))
    sin_U1 = tan_U1 * cos_U1
    o1 = math.atan2(tan_U1, cos_angle1)
    sin_angle = cos_U1 * sin_angle1
    cosSq_angle = 1 - sin_angle*sin_angle
    uSq = cosSq_angle * (a*a - b*b) / (b*b)
    A = 1 + uSq / 16384 * (4096 + uSq * (-768 + uSq * (320 - 175 * uSq)))
    B = uSq / 1024 * (256 + uSq * (-128 + uSq * (74 - 47 * uSq)))

    r1 = d / (b*A)
    r2 = 0.0
    iteration = 0
    while abs(r1 - r2 > 1e-12) and iteration < 100:
        cos2roM = math.cos(2*o1 + r1)
        sin_ro = math.sin(r1)
        cos_ro = math.cos(r1)
        dif_ro = B * sin_ro * (cos2roM + B/4 * (cos_ro * (-1 + 2*cos2roM*cos2roM) -
                                                B/6 * cos2roM * (-3 +4*sin_ro*sin_ro) *
                                                (-3 + 4*cos2roM*cos2roM)))
        r2 = r1
        r1 = d / (b*A) + dif_ro
        iteration += 1

    x = sin_U1*sin_ro - cos_U1*cos_ro*cos_angle1
    lon = math.atan2(sin_ro*sin_angle1, cos_U1*cos_ro - sin_U1*sin_ro*cos_angle1)
    C = f/16 * cosSq_angle*(4+f * (4 - 3*cosSq_angle))
    L = lon - (1-C) * f * sin_angle * (r1 + C*sin_ro * (cos2roM+C*cos_ro * (-1 + 2*cos2roM*cos2roM)))

    end_lon = (start_lon + L + 3*math.pi) % (2*math.pi) - math.pi
    end_lat = math.atan2(sin_U1 * cos_ro + cos_U1 * sin_ro * cos_angle1, (1 - f) * math.sqrt(sin_angle * sin_angle + x * x))

    return end_lon * 180 / math.pi, end_lat * 180 / math.pi


def distance_plane(p1_x, p1_y, p2_x, p2_y):
    """
    Finds the distance between two points in a plane
    :param p1_x: The first point x
    :param p1_y: The first point y
    :param p2_x: The second point x
    :param p2_y: The second point y
    :type p1_x: float
    :type p1_y: float
    :type p2_x: float
    :type p2_y: float
    :return: Return distance between p1 and p2
    :rtype: float
    """
    vect_x = p2_x - p1_x
    vect_y = p2_y - p1_y
    return math.sqrt(vect_x ** 2 + vect_y ** 2)


def project_point_to_line(point_x, point_y, line_start_x, line_start_y, line_end_x, line_end_y):
    """
    Projects a point onto a line where the normal of the line that passes through the point intersects with the line
    :param point_x: The point that will be projected on the line (x coordinate)
    :param point_y: The point that will be projected on the line (y coordinate)
    :param line_start_x: The first point of line (x coordinate)
    :param line_start_y: The first point of line (y coordinate)
    :param line_end_x: The second point of line (x coordinate)
    :param line_end_y: The second point of line (y  coordinate)
    :type point_x: float
    :type point_y: float
    :type line_start_x: float
    :type line_start_y: float
    :type line_end_x: float
    :type line_end_y: float
    :return: return the point projected onto the line
    :rtype: [float, float]
    """
    if (point_x == line_start_x and point_y == line_start_y) or (point_x == line_end_x and point_y == line_end_y):
        return [point_x, point_y]

    # distances between vertex
    start_to_p = distance_plane(line_start_x, line_start_y, point_x, point_y)
    end_to_p = distance_plane(line_end_x, line_end_y, point_x, point_y)
    start_to_end = distance_plane(line_start_x, line_start_y, line_end_x, line_end_y)
    if start_to_end == 0:
        # start and end are the same point, cant form a line
        return line_start_x, line_start_y  # Projection of a point onto another point is the point itself
    # cosines theorem, angle in radians
    r = ((end_to_p ** 2) - (start_to_p ** 2) - (start_to_end ** 2)) / (-2 * start_to_p * start_to_end)
    if r > 1:
        r = 1
    elif r < -1:
        r = -1
    angle_s = math.acos(r)

    # get small angle of vertex-point
    angle_p_small = math.pi/2 - angle_s
    # get distance between start and projection
    c = start_to_p * math.sin(angle_p_small)

    # get projection position from line
    xp = line_start_x + (c / start_to_end) * (line_end_x - line_start_x)
    yp = line_start_y + (c / start_to_end) * (line_end_y - line_start_y)

    return xp, yp


def distance_to_segment(point_x, point_y, segment_start_x, segment_start_y, segment_end_x, segment_end_y):
    """
    Computes the minimum distance between a point C and a line segment with endpoints A and B.
    :param point_x: The point that will be projected on the line (x coordinate)
    :param point_y: The point that will be projected on the line (y coordinate)
    :param segment_start_x: The first point of line (x coordinate)
    :param segment_start_y: The first point of line (y coordinate)
    :param segment_end_x: The second point of line (x coordinate)
    :param segment_end_y: The second point of line (y  coordinate)
    :type point_x: float
    :type point_y: float
    :type segment_start_x: float
    :type segment_start_y: float
    :type segment_end_x: float
    :type segment_end_y: float
    :return: minimum distance between point and line segment
    :rtype: float
    """
    p_x, p_y = project_point_to_line(point_x, point_y,
                              segment_start_x, segment_start_y,
                              segment_end_x, segment_end_y)

    if is_between(p_x, p_y,
                  segment_start_x, segment_start_y,
                  segment_end_x, segment_end_y):
        return distance_plane(point_x, point_y,
                              p_x, p_y)

    else:
        d1 = distance_plane(point_x, point_y,
                            segment_start_x, segment_start_y)
        d2 = distance_plane(point_x, point_y,
                            segment_end_x, segment_end_y)
        if d1 < d2:
            return d1
        else:
            return d2


def is_between(a_x, a_y, b_x, b_y, c_x, c_y):
    """
    Calculate if point A is between points B and C. In other words, a straight line passes through all 3 points
    :param a_x: point A (X coordinate)
    :type a_x: float
    :param a_y: point A (Y coordinate)
    :type a_y: float
    :param b_x: point B (X coordinate)
    :type b_x: float
    :param b_y: point B (Y coordinate)
    :type b_y: float
    :param c_x: point C (X coordinate)
    :type c_x: float
    :param c_y: point C (Y coordinate)
    :type c_y: float
    :return: return True if A is between B and C, otherwise False
    :rtype: bool
    """
    cross_product = (a_y - b_y) * (c_x - b_x) - (a_x - b_x) * (c_y - b_y)
    if abs(cross_product) == 0:
        dist_a_to_b = math.sqrt((a_x - b_x) ** 2 + (a_y - b_y) ** 2)
        dist_a_to_c = math.sqrt((a_x - c_x) ** 2 + (a_y - c_y) ** 2)
        dist_b_to_c = math.sqrt((c_x - b_x) ** 2 + (c_y - b_y) ** 2)
        if not math.isclose(dist_a_to_b + dist_a_to_c, dist_b_to_c):  # to better compare floats, use isclose()
            return False

    dot_product = (a_x - b_x) * (c_x - b_x) + (a_y - b_y) * (c_y - b_y)
    if dot_product < 0:
        return False

    squared_length_ba = (c_x - b_x) ** 2 + (c_y - b_y) ** 2
    if dot_product > squared_length_ba:
        return False
    return True
