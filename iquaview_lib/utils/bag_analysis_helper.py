"""
Copyright (c) 2020 Iqua Robotics SL

# Distributed under the terms of the Modified BSD License (BSD 3-clause)
# see 'LICENSE.txt', which is part of this source code package.
"""

import hashlib
import rospy


def iterate_dictionary(field_topic_dict, dictionary, topic, topic_field):
    """
    iterate over dictionary to set a relation between field and topic
    :param field_topic_dict: dictionary where key is a field of a topic and content is the name of the topic
    :param dictionary: dictionary to read
    :param topic: topic name
    :param topic_field: topic field name
    """
    for key, value in dictionary.items():
        if topic_field.endswith('/stamp'):
            field_topic_dict[str(topic_field)] = topic
        # recursive call
        elif isinstance(value, dict):
            topic_field_and_key = topic_field + "/" + key
            iterate_dictionary(field_topic_dict, value, topic, topic_field_and_key)
            continue

        if isinstance(value, list):
            for idx, field in enumerate(value):
                field_topic_dict[str(topic_field + "/" + key + "." + str(idx))] = topic
        else:
            field_topic_dict[str(topic_field + "/" + key)] = topic

def get_topic_field_with_namespace(vehicle_namespace, topic_field):
    """
    Get topic_field with namespace. Ex: /girona500/navigator/navigation/position/depth
    :param vehicle_namespace: vehicle namespace
    :param topic_field: name of the field
    :return: return topic_field with namespace
    """
    name = "/{}/{}".format(vehicle_namespace, topic_field)
    return name

def get_field_without_topic_name(field_topic_dict, topic_field):
    """
    get a field name without a topic name
    :param field_topic_dict: dictionary where key is a field of a topic and content is the name of the topic
    :param topic_field: full name of the field
    :return: return field name without topic name
    """
    field = topic_field.replace(field_topic_dict.get(topic_field), '')
    return field

def get_message_value(fields, json_structure):
    """
     from json explore fields to get a value
    :param fields: list of fields
    :type fields: list
    :param json_structure: topic message as a json
    :type json_structure: json
    :return: return value of the field
    """
    for field in fields:
        if field != "":
            if "." in field:
                field_name, idx = field.split('.')
                json_structure = json_structure[field_name]
                value = json_structure[int(idx)]
            else:
                if field == "stamp":
                    # if field is stamp, read time and convert to secs
                    secs = json_structure[field]['secs']
                    nsecs = json_structure[field]['nsecs']
                    time = rospy.Time(secs, nsecs)
                    value = time.to_sec()
                else:
                    json_structure = json_structure[field]
                    value = json_structure
    return value


def get_md5(file_name):
    """
     Check data integrity
    :param file_name: name of the file to check
    :type file_name: str
    :return: return HEX string representing the hash
    :rtype: str
    """
    return hashlib.md5(open(file_name,'rb').read()).hexdigest()
