# IQUAview LIB

Reusable code that is used all over IQUAview and IQUAview plugins.

## Installation

To **install** Python packages to the system, go to the `iquaview_lib` folder and type:

```bash
pip3 install .
```

To **uninstall** the package:

```bash
pip3 uninstall iquaview_lib
```
