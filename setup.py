#!/usr/bin/env python

from distutils.core import setup

setup(name='iquaview_lib',
      version='20.10.6',
      description='IQUAview lib',
      author='Iqua Robotics',
      author_email='software@iquarobotics.com',
      packages=['iquaview_lib',
                'iquaview_lib.auv_configs',
                'iquaview_lib.cola2api',
                'iquaview_lib.connection',
                'iquaview_lib.connection.sftp',
                'iquaview_lib.ui',
                'iquaview_lib.utils',
                'iquaview_lib.vehicle',
                'iquaview_lib.xmlconfighandler',
                'iquaview_lib.ui'],
      include_package_data=True,
      package_data={"iquaview_lib": ['*.xml'],
                    "iquaview_lib": ['auv_configs/*.xml']},

      )

